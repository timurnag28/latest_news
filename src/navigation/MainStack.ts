import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';
import MainScreen from '../screens/main/MainScreen';
import SideBar from '../screens/main/components/sideBar/SideBar';
import ShopPage from '../screens/main/components/shopPage/ShopPage';
import PaymentPage from '../screens/main/components/shopPage/paymentPage/PaymentPage';
import MapPage from '../screens/main/components/shopPage/paymentPage/MapPage';
import BasketPage from '../screens/main/components/shopPage/basketPage/BasketPage';
import {CloudPayment} from '../screens/main/components/shopPage/paymentPage/CloudPayment';
import MyData from '../screens/main/components/sideBar/MyData';

export default createStackNavigator(
  {
    MainScreen: {
      screen: MainScreen,
      navigationOptions: {
        headerTitle: 'Главная',
      },
    },
    SideBar: {
      screen: SideBar,
      navigationOptions: {
        headerTitle: 'Сайдбар',
      },
    },
    MyData: {
      screen: MyData,
      navigationOptions: {
        headerTitle: 'Мои данные',
      },
    },
    ShopPage: {
      screen: ShopPage,
      navigationOptions: {
        headerTitle: 'Магазин',
      },
    },
    BasketPage: {
      screen: BasketPage,
      navigationOptions: {
        headerTitle: 'Корзина',
      },
    },

    PaymentPage: {
      screen: PaymentPage,
      navigationOptions: {
        headerTitle: 'Оплата',
      },
    },
    MapPage: {
      screen: MapPage,
      navigationOptions: {
        headerTitle: 'Карта',
      },
    },
    CloudPayment: {
      screen: CloudPayment,
      navigationOptions: {
        headerTitle: 'CloudPayment заставка',
      },
    },
  },
  {
    initialRouteName: 'MainScreen',
    defaultNavigationOptions: {
      header: () => null,
      ...TransitionPresets.ModalSlideFromBottomIOS,
      cardStyle: {
        backgroundColor: '#fff',
      },
    },
  },
);
