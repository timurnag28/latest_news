import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import LoginStack from './LoginStack';
import MainStack from './MainStack';
import CourierStack from './CourierStack';

const AppNavigator = createSwitchNavigator(
  {
    loginStack: LoginStack,
    mainStack: MainStack,
    courierStack: CourierStack,
  },

  {
    initialRouteName: 'loginStack',
  },
);
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
