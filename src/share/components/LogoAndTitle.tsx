import {StyleProp, Text, TouchableOpacity, View, ViewStyle} from 'react-native';
import React from 'react';
import {MontserratBold} from '../fonts';
import {size14, size24, size34} from '../consts';
import FastImage from 'react-native-fast-image';

export const LogoAndTitle = ({
  style,
  imageSize,
  textSize,
  courier,
}: {
  style?: StyleProp<ViewStyle>;
  imageSize: number;
  textSize: number;
  courier?: boolean;
}) => {
  return (
    <View style={[{flexDirection: 'row', alignItems: 'center'}, style]}>
      {!courier ? (
        <FastImage
          source={require('../../../assets/images/logo.png')}
          style={{width: imageSize, height: imageSize}}
        />
      ) : (
        <FastImage
          source={require('../../../assets/images/grayLogo.png')}
          style={{width: imageSize, height: imageSize}}
        />
      )}
      <View style={{flexDirection: 'column', paddingLeft: 6}}>
        <Text
          style={{
            fontSize: textSize,
            fontFamily: MontserratBold,
            color: !courier ? 'black' : 'gray',
          }}>
          Свежие
        </Text>
        <Text
          style={{
            paddingLeft: !courier ? 6 : 0,
            fontSize: textSize,
            fontFamily: MontserratBold,
            color: !courier ? 'black' : 'gray',
          }}>
          новости
        </Text>
        {courier ? (
          <Text
            style={{
              fontSize: size14 / 2,
              fontFamily: MontserratBold,
              color: !courier ? 'black' : 'gray',
            }}>
            доставка
          </Text>
        ) : null}
      </View>
    </View>
  );
};
