import React, {Component} from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import {HEADER_HEIGHT, size34, WINDOW_HEIGHT} from '../consts';
import SideBar from '../../screens/main/components/sideBar/SideBar';

interface HeaderProps {
  style?: StyleProp<ViewStyle>;
  headerRight?: object;
  headerLeft?: object;
  marginTopHeader?: number;
  headerMid?: object;
  onAuth: () => void;
}

export default class Header extends Component<HeaderProps> {
  render() {
    const {
      style,
      headerLeft,
      headerRight,
      marginTopHeader,
      headerMid,
    } = this.props;
    return (
      <SafeAreaView
        style={[
          {
            backgroundColor: 'transparent',
            zIndex: 100,
            borderWidth: 0.4,
            borderColor: 'rgba(0, 0, 0, 0.2)',
            position: 'absolute',
            height: WINDOW_HEIGHT,
            left: 0,
            right: 0,
          },
          style,
        ]}>
        <View
          style={{
            marginTop: marginTopHeader,
            height: HEADER_HEIGHT * 1.5,
            backgroundColor: 'white',
            paddingTop: size34 * 2,
          }}>
          {headerLeft ? (
            <View
              style={[
                {
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  left: 16,
                  alignSelf: 'center',
                  justifyContent: 'center',
                },
              ]}>
              {headerLeft}
            </View>
          ) : null}
          {headerMid ? (
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                top: 0,
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              {headerMid}
            </View>
          ) : null}
          {headerRight ? (
            <View
              style={{
                position: 'absolute',
                right: 0,
                bottom: 0,
                top: 0,
                justifyContent: 'center',
              }}>
              {headerRight}
            </View>
          ) : null}
        </View>
        <SideBar onAuth={this.props.onAuth}/>
      </SafeAreaView>
    );
  }
}
