import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

import Header from './Header';

import {LogoAndTitle} from './LogoAndTitle';
import store from '../../stores';

import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import {observer} from 'mobx-react';
import {size12, size34} from '../consts';

@observer
export default class MainHeader extends React.Component {
  render() {
    const {onChangeView, isShowSideBar} = store.modalsStore;
    return (
      <Header
        onAuth={this.props.onAuth}
        headerLeft={
          !isShowSideBar ? (
            <TouchableOpacity style={{marginLeft: 8}} onPress={onChangeView}>
              <Feather
                name={'menu'}
                size={size34}
                color={'rgba(112, 112, 112, 0.4)'}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={onChangeView}>
              <EvilIcons name={'close'} size={size34 * 1.5} color={'#464646'} />
            </TouchableOpacity>
          )
        }
        headerMid={<LogoAndTitle imageSize={size34} textSize={size12} />}
      />
    );
  }
}
const styles = StyleSheet.create({});
