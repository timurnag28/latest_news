import React from 'react';
import {
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {observer} from 'mobx-react';
import {NavigationProps} from '../../share/interfaces';
import Header from '../../share/components/Header';
import Feather from 'react-native-vector-icons/Feather';
import {
  size12,
  size16,
  size28,
  size34,
  size44,
  WINDOW_WIDTH,
} from '../../share/consts';
import {LogoAndTitle} from '../../share/components/LogoAndTitle';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {orders, takeOrders} from '../../share/info';
import {ListItem} from './components/ListItem';
import {MontserratSemiBold} from '../../share/fonts';

@observer
export default class TakeOrderScreen extends React.Component<NavigationProps> {
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <Header
          onAuth={this.props.onAuth}
          headerLeft={
            <TouchableOpacity
              style={{marginLeft: 8}}
              onPress={() => this.props.navigation.navigate('CourierProfile')}>
              <Feather
                name={'menu'}
                size={size34}
                color={'rgba(112, 112, 112, 0.4)'}
              />
            </TouchableOpacity>
          }
          headerMid={
            <LogoAndTitle courier={true} imageSize={size34} textSize={size12} />
          }
          headerRight={
            <TouchableOpacity
              style={{marginRight: 28}}
              onPress={() => this.props.navigation.navigate('TakeOrderScreen')}>
              <FontAwesome5
                name={'map-marker'}
                size={size28}
                color={'#8CC83F'}
              />
            </TouchableOpacity>
          }
        />
        {/*<Text style={{fontFamily: MontserratSemiBold, fontSize: size16}}>*/}
        {/*  Нет активных заказов*/}
        {/*</Text>*/}
        <View
          style={{
            marginTop: size44 * 2,
            width: WINDOW_WIDTH,
          }}>
          <SectionList
            showsVerticalScrollIndicator={false}
            sections={takeOrders}
            keyExtractor={(item, index) => item + index}
            renderItem={({item}) => (
              <ListItem
                takeOrderType={true}
                item={item}
                onPress={() => this.props.navigation.navigate('CourierScreen')}
              />
            )}
            renderSectionHeader={({section: {title}}) => (
              <View
                style={{backgroundColor: '#8CC83F', paddingVertical: size16}}>
                <Text
                  style={{
                    fontFamily: MontserratSemiBold,
                    fontSize: size16,
                    paddingHorizontal: size16,
                    color: '#FFFFFF',
                  }}>
                  {title}
                </Text>
              </View>
            )}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({});
