import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  size12,
  size16,
  size20,
  size28,
  size34,
  size36,
  size44,
  WINDOW_WIDTH,
} from '../../../share/consts';
import {
  MontserratBold,
  MontserratRegular,
  MontserratSemiBold,
} from '../../../share/fonts';
import Entypo from 'react-native-vector-icons/Entypo';
import {ClientAddress} from './ClientAddress';
import {ActionButton} from '../../../share/components/ActionButton';
import {PhoneComponent} from './PhoneComponent';

interface IProps {
  shopName: string;
  shopAddress: string;
  phone: string;
  time: string;
  address: string;
  porch: string;
  floor: string;
  apartment: string;
  intercom: string;
  comment: string;
}

export const ListItem = ({
  item,
  onPress,
  takeOrderType,
}: {
  item: IProps;
  onPress: () => void;
  takeOrderType?: boolean;
}) => {
  return (
    <View
      style={{
        marginBottom: size44,
        paddingHorizontal: size16,
        paddingTop: size28,
      }}>
      <Text style={{fontSize: size20}}>
        <Text style={{fontFamily: MontserratRegular}}>Из</Text>{' '}
        <Text style={{fontFamily: MontserratBold, color: '#8CC83F'}}>
          {item.shopName}
        </Text>
      </Text>
      <Text style={{fontFamily: MontserratRegular, paddingTop: size12}}>
        {item.shopAddress}
      </Text>
      <Text
        onPress={() => alert('карта')}
        style={{
          fontFamily: MontserratRegular,
          paddingTop: size12,
          textDecorationLine: 'underline',
        }}>
        Показать на карте
      </Text>
      <PhoneComponent phone={item.phone} />
      <View
        style={{
          height: 1,
          backgroundColor: 'rgba(112, 112, 112, .2)',
          marginVertical: size20,
        }}
      />
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Text
          style={{
            color: '#8CC83F',
            fontFamily: MontserratBold,
            fontSize: size20,
          }}>
          Клиенту
        </Text>
        <Text
          style={{
            color: '#000000',
            fontFamily: MontserratRegular,
            fontSize: size20,
            marginLeft: 9,
          }}>
          за
        </Text>
        <Text
          style={{
            color: item.time >= 10 ? '#000000' : 'red',
            fontFamily: MontserratBold,
            fontSize: size20,
            marginLeft: 9,
          }}>
          {item.time}
        </Text>
        <Text
          style={{
            color: item.time >= 10 ? '#000000' : 'red',
            fontFamily: MontserratBold,
            fontSize: size20,
            marginLeft: 9,
          }}>
          мин
        </Text>
        {item.time <= 10 ? (
          <Entypo
            name={'warning'}
            size={size20}
            color={'red'}
            style={{paddingLeft: 9}}
          />
        ) : null}
      </View>
      <ClientAddress item={item} />
      <Text
        onPress={() => alert('карта')}
        style={{
          fontFamily: MontserratRegular,
          paddingTop: size12,
          textDecorationLine: 'underline',
        }}>
        Показать на карте
      </Text>
      <Text
        style={{
          fontFamily: MontserratSemiBold,
          fontSize: size16,
          paddingTop: size28,
        }}>
        Комментарий
      </Text>
      <Text
        style={{
          fontSize: size12,
          fontFamily: MontserratRegular,
          paddingTop: size16,
        }}>
        {item.comment}
      </Text>
      {!takeOrderType ? (
        <ActionButton
          style={{marginTop: 24, width: WINDOW_WIDTH * 0.9}}
          onPress={onPress}
          text={'Заказ доставлен'}
          textStyle={{fontSize: size12}}
        />
      ) : (
        <View
          style={{
            flexDirection: 'row',
            marginTop: size34,
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            onPress={onPress}
            style={{
              paddingVertical: size16,
              backgroundColor: '#8CC83F',
              borderRadius: 10,
              flex: 1,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#FFFFFF',
                fontFamily: MontserratRegular,
                fontSize: size12,
                paddingHorizontal: size36,
              }}>
              Взять заказ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onPress();
              alert('отказ от заказа');
            }}
            style={{
              paddingVertical: size16,
              backgroundColor: '#AAA8A8',
              borderRadius: 10,
              marginLeft: size16,
              flex: 1,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#FFFFFF',
                fontFamily: MontserratRegular,
                fontSize: size12,
                paddingHorizontal: size44,
              }}>
              Отказаться
            </Text>
          </TouchableOpacity>
        </View>
      )}
      <ActionButton
        style={{
          paddingVertical: 16,
          backgroundColor: '#F5F4F4',
          borderRadius: 10,
          width: WINDOW_WIDTH * 0.9,
          alignItems: 'center',
          marginTop: size16,
        }}
        onPress={() => alert('Связаться с менеджером')}
        text={'Связаться с менеджером'}
        textStyle={{
          color: '#000000',
          fontFamily: MontserratSemiBold,
          fontSize: size12,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({});
