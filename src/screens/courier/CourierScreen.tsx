import React from 'react';
import {
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {observer} from 'mobx-react';
import {NavigationProps} from '../../share/interfaces';
import {
  size12,
  size14,
  size16,
  size20,
  size28,
  size34,
  size44,
  WINDOW_WIDTH,
} from '../../share/consts';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {LogoAndTitle} from '../../share/components/LogoAndTitle';
import Header from '../../share/components/Header';
import {orders} from '../../share/info';
import {MontserratSemiBold} from '../../share/fonts';
import {ListItem} from './components/ListItem';
import store from '../../stores';

@observer
export default class CourierScreen extends React.Component<NavigationProps> {
  render() {
    const {onShowCourierProfileModal} = store.modalsStore;
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <Header
          onAuth={this.props.onAuth}
          headerLeft={
            <TouchableOpacity
              style={{marginLeft: 8}}
              onPress={() => this.props.navigation.navigate('CourierProfile')}>
              <Feather
                name={'menu'}
                size={size34}
                color={'rgba(112, 112, 112, 0.4)'}
              />
            </TouchableOpacity>
          }
          headerMid={
            <LogoAndTitle courier={true} imageSize={size34} textSize={size12} />
          }
          headerRight={
            <TouchableOpacity
              style={{marginRight: 28}}
              onPress={() => this.props.navigation.navigate('TakeOrderScreen')}>
              <FontAwesome5
                name={'map-marker'}
                size={size28}
                color={'#8CC83F'}
              />
            </TouchableOpacity>
          }
        />
        {/*<Text style={{fontFamily: MontserratSemiBold, fontSize: size16}}>*/}
        {/*  Нет активных заказов*/}
        {/*</Text>*/}
        <View
          style={{
            marginTop: size44 * 2,
            width: WINDOW_WIDTH,
          }}>
          <SectionList
            showsVerticalScrollIndicator={false}
            sections={orders}
            keyExtractor={(item, index) => item + index}
            renderItem={({item}) => (
              <ListItem
                item={item}
                onPress={() =>
                  this.props.navigation.navigate('ConfirmScreen', {
                    item: item,
                  })
                }
              />
            )}
            renderSectionHeader={({section: {title}}) => (
              <View
                style={{backgroundColor: '#F5F4F4', paddingVertical: size16}}>
                <Text
                  style={{
                    fontFamily: MontserratSemiBold,
                    fontSize: size16,
                    paddingHorizontal: size16,
                  }}>
                  {title}
                </Text>
              </View>
            )}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({});
