import React from 'react';
import {Animated, BackHandler, TouchableOpacity, View} from 'react-native';
import {NavigationProps} from '../../share/interfaces';
import {observer} from 'mobx-react';
import ShopsList from './components/shops/ShopsList';
import {HEADER_HEIGHT, WINDOW_WIDTH} from '../../share/consts';
import {CustomInput} from '../../share/components/CustomInput';
import store from '../../stores';
import MainHeader from '../../share/components/MainHeader';

@observer
export default class MainScreen extends React.Component<NavigationProps> {
  backHandler: any;
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackPress,
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleBackPress = () => {
    const {onChangeView, isShowSideBar} = store.modalsStore;
    isShowSideBar ? onChangeView() : null;
    return isShowSideBar ? true : null;
  };

  render() {
    const {
      animatedValue,
      isShowBackgroundInput,
      clientAddress,
      onChangeClientAddress,
    } = store.shopsStore;
    const {onChangeView, onCloseSideBarAndShowAuth} = store.modalsStore;

    const viewOpacity = animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 0.5, 1],
    });
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <View style={{height: HEADER_HEIGHT}} />
        <MainHeader
          onAuth={() => {
            onCloseSideBarAndShowAuth();
            setTimeout(() => this.props.navigation.navigate('Login'), 400);
          }}
        />
        <Animated.View
          style={{
            top: WINDOW_WIDTH / 4,
            position: 'absolute',
            zIndex: 1,
            opacity: viewOpacity,
          }}>
          <CustomInput
            editable={isShowBackgroundInput}
            placeholderTextColor={'#000000'}
            textInputStyle={{
              width: WINDOW_WIDTH / 1.2,
            }}
            style={{
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 1,
              },
              shadowOpacity: 0.18,
              shadowRadius: 1.0,
              elevation: 1,
              backgroundColor: '#FFFFFF',
            }}
            placeholder={'Адрес'}
            value={clientAddress}
            onChangeText={item => onChangeClientAddress(item)}
          />
        </Animated.View>
        <ShopsList navigation={this.props.navigation} />
      </View>
    );
  }
}
// const styles = StyleSheet.create({});
