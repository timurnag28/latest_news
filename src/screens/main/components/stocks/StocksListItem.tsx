import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {size16, size28} from '../../../../share/consts';

export const StocksListItem = ({
  style,
  keyIndex,
}: {
  style?: StyleProp<ViewStyle>;
  keyIndex: number;
}) => {
  return (
    <LinearGradient
      key={keyIndex}
      useAngle
      angle={140}
      locations={[0, 1]}
      colors={['#A6ED4A', '#8AC83E']}
      style={[
        {
          width: 300,
          height: 150,
          borderRadius: 20,
          justifyContent: 'flex-end',
        },
        style,
      ]}>
      <View
        key={keyIndex}
        style={{
          flexDirection: 'column',
          paddingLeft: 16,
          paddingBottom: 16,
        }}>
        <Text
          style={{
            fontSize: size16,
            color: '#fafafa',
          }}>
          ЯНВАРЬ
        </Text>
        <Text
          style={{
            fontSize: size28,
            color: '#fafafa',
          }}>
          -30%
        </Text>
        <Text
          style={{
            color: '#fafafa',
            fontSize: size28,
          }}>
          НА ВСЕ
        </Text>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({});
