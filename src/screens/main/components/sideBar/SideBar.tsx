import React from 'react';
import {Animated, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {observer} from 'mobx-react';
import {MenuTitle} from '../MenuTitle';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {MontserratRegular} from '../../../../share/fonts';
import {
  HEADER_HEIGHT,
  size14,
  size16,
  size20,
  size28,
  size34,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../../../share/consts';
import store from '../../../../stores';
import {BottomContent} from './BottomContent';
import {NavigationProps} from "../../../../share/interfaces";

@observer
export default class SideBar extends React.Component<NavigationProps> {
  render() {
    const {
      animatedValue,
      onChangeView,
      onCloseViewAndShowMyDataModal,
      onCloseSideBarAndShowAuth,
    } = store.modalsStore;
    const {isAuth} = store.authStore;
    const viewSideBarOpacity = animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 0.5, 1],
    });
    const viewSideBarHeight = animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, WINDOW_HEIGHT / 2, WINDOW_HEIGHT],
    });
    return (
      <Animated.View
        style={{
          width: WINDOW_WIDTH,
          height: viewSideBarHeight,
          marginTop: HEADER_HEIGHT * 1.5,
          position: 'absolute',
          zIndex: 3,
          opacity: viewSideBarOpacity,
        }}>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            flex: !isAuth ? 3 : 2,
            justifyContent: 'space-between',
          }}>
          <View style={{paddingLeft: 24, paddingTop: size34}}>
            <MenuTitle titleStyle={styles.menuTitle} title={'Стать курьером'} />
            <MenuTitle
              titleStyle={styles.menuTitle}
              title={'Юридическим лицам'}
            />
            <MenuTitle titleStyle={styles.menuTitle} title={'Доставка'} />
            <MenuTitle
              titleStyle={styles.menuTitle}
              title={'Вопросы и ответы'}
            />
            <MenuTitle titleStyle={styles.menuTitle} title={'Обратная связь'} />
            <MenuTitle
              titleStyle={styles.menuTitle}
              title={'Пользовательское соглашение'}
            />
            {!isAuth ? (
              <BottomContent onPress={onCloseViewAndShowMyDataModal} />
            ) : null}
          </View>
          {!isAuth ? (
            <View style={styles.footerTitle}>
              <FontAwesome5
                name={'shopping-cart'}
                size={size16}
                color={'#8cc83f'}
              />
              <Text style={styles.bottomMenuTitle}>Магазины</Text>
            </View>
          ) : (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                  flex: 1,
                  backgroundColor: 'rgba(245, 244, 244, 0.5)',
                  paddingVertical: size34,
                }}>
                <FontAwesome5
                  name={'shopping-cart'}
                  size={size16}
                  color={'#8cc83f'}
                />
                <Text style={styles.bottomMenuTitle}>Магазины</Text>
              </View>
              <TouchableOpacity
                onPress={this.props.onAuth}
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                  flex: 1,
                  backgroundColor: '#F5F4F4',
                  paddingVertical: size34,
                }}>
                <FontAwesome5
                  name={'shopping-cart'}
                  size={size16}
                  color={'#8cc83f'}
                />
                <Text style={styles.bottomMenuTitle}>Вход</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <TouchableOpacity
          activeOpacity={1}
          onPress={onChangeView}
          style={{
            flex: 1,
            backgroundColor: 'rgba(0, 0, 0, 0.3)',
          }}
        />
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  menuTitle: {color: '#000000', fontSize: size14, paddingTop: size20},
  bottomMenuContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: size28,
  },
  bottomMenuTitle: {
    fontSize: size16,
    color: '#000000',
    paddingLeft: 16,
    fontFamily: MontserratRegular,
  },
  footerTitle: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingBottom: size34,
  },
});
