import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

import {observer} from 'mobx-react';
import {MontserratRegular, MontserratSemiBold} from '../../../../share/fonts';
import {
  size12,
  size14,
  size20,
  size28,
  size34,
  size44,
  WINDOW_WIDTH,
} from '../../../../share/consts';
import {CustomInput} from '../../../../share/components/CustomInput';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {Toggle} from '../../../../share/components/Toggle';
import store from '../../../../stores';

@observer
export default class MyData extends Component {
  state = {
    name: 'Василий',
    phone: '+7 912 345 67 89',
    mail: 'vasiliy@mail.ru',
    alertsIsOn: true,
  };

  alertsToggleHandle(state: boolean) {
    this.setState({alertsIsOn: state});
  }
  render() {
    const {onShowMyDataModal} = store.modalsStore;
    const {name, phone, mail} = this.state;
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={onShowMyDataModal}
        style={{
          backgroundColor: 'rgba(0, 0, 0, 0.3)',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          activeOpacity={1}
          style={{
            backgroundColor: '#ffffff',
            borderRadius: 20,
            paddingHorizontal: 16,
            width: WINDOW_WIDTH * 0.9,
            alignItems: 'center',
          }}>
          <EvilIcons
            onPress={onShowMyDataModal}
            name={'close'}
            size={size28}
            color={'#707070'}
            style={{position: 'absolute', right: 20, top: 20, zIndex: 100}}
          />
          <View style={{width: WINDOW_WIDTH * 0.9, paddingHorizontal: 15}}>
            <Text
              style={{
                color: '#BABABA',
                fontFamily: MontserratSemiBold,
                fontSize: size20,
                paddingTop: size34,
                paddingBottom: size28,
              }}>
              Мои данные
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <Text
                style={{
                  fontFamily: MontserratRegular,
                  fontSize: size14,
                  flex: 0.7,
                }}>
                Имя
              </Text>
              <Text
                style={{
                  fontFamily: MontserratRegular,
                  fontSize: size14,
                  flex: 1,
                }}>
                Телефон
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <CustomInput
                value={name}
                onChangeText={value => this.setState({name: value})}
                textInputStyle={{
                  flex: 1,
                  fontFamily: MontserratRegular,
                  fontSize: size14,
                  backgroundColor: '#F5F4F4',
                  borderRadius: 10,
                }}
                style={{
                  marginTop: 16,
                  flex: 1,
                  marginRight: 14,
                }}
              />
              <CustomInput
                keyboardType={'numeric'}
                value={phone}
                onChangeText={value => this.setState({phone: value})}
                textInputStyle={{
                  flex: 1,
                  fontFamily: MontserratRegular,
                  fontSize: size14,
                  backgroundColor: '#F5F4F4',
                  borderRadius: 10,
                }}
                style={{
                  marginTop: 16,
                  flex: 1,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: MontserratRegular,
                fontSize: size14,
                paddingTop: 15,
              }}>
              E-mail
            </Text>
            <CustomInput
              value={mail}
              onChangeText={value => this.setState({mail: value})}
              textInputStyle={{
                flex: 1,
                fontFamily: MontserratRegular,
                fontSize: size14,
                backgroundColor: '#F5F4F4',
                borderRadius: 10,
              }}
              style={{
                marginTop: 15,
              }}
            />
            <Text
              style={{
                color: '#BABABA',
                fontFamily: MontserratSemiBold,
                fontSize: size20,
                paddingTop: size34,
                paddingBottom: size34,
              }}>
              Адреса
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text
                style={{
                  fontFamily: MontserratRegular,
                  fontSize: size14,
                  flex: 0.8,
                }}>
                Ленинская площадь восстания, 42 202 кв. 8 пад. 5 эт.
              </Text>
              <EvilIcons
                onPress={() => alert('удаление адреса')}
                name={'close'}
                size={size28}
                color={'#707070'}
                style={{paddingLeft: 16}}
              />
            </View>
            <View style={{flexDirection: 'row', paddingTop: 24}}>
              <Text
                style={{
                  fontFamily: MontserratRegular,
                  fontSize: size14,
                  flex: 0.8,
                }}>
                Ленинский проспект, 12 202 офис. 8 пад. 5 эт.
              </Text>
              <EvilIcons
                onPress={() => alert('удаление адреса')}
                name={'close'}
                size={size28}
                color={'#707070'}
                style={{paddingLeft: 16}}
              />
            </View>
            <TouchableOpacity onPress={() => alert('добавление адреса')}>
              <Text
                style={{
                  fontFamily: MontserratSemiBold,
                  fontSize: size14,
                  color: '#8CC83F',
                  paddingTop: 24,
                }}>
                Добавить адрес
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
                paddingTop: size44,
                justifyContent: 'space-between',
                marginBottom: 16,
              }}>
              <Text
                style={{
                  fontSize: size12,
                  fontFamily: MontserratRegular,
                  flex: 0.8,
                }}>
                Получать информацию об акциях, скидках и новых предложениях
              </Text>
              <View>
                <Toggle
                  isOn={this.state.alertsIsOn}
                  onToggle={state => this.alertsToggleHandle(state)}
                />
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => alert('Сохранение данных')}
            style={{
              backgroundColor: '#8CC83F',
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              paddingVertical: size28,
              justifyContent: 'center',
              alignItems: 'center',
              width: WINDOW_WIDTH * 0.9 + 1,
              flex: 1,
            }}>
            <Text style={{color: '#FFFFFF'}}>Сохранить</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
}

// const styles = StyleSheet.create({});
