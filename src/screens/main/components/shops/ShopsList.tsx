import React from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {observer} from 'mobx-react';
import store from '../../../../stores';
import {MontserratRegular} from '../../../../share/fonts';
import {categories, data} from '../../../../share/info';
import {ShopsListItem} from './ShopsListItem';
import {size16} from '../../../../share/consts';
import {NavigationProps} from '../../../../share/interfaces';
import FooterComponent from '../footer/FooterComponent';
import {HeaderText} from '../HeaderText';
import {StocksListItem} from '../stocks/StocksListItem';
import HeaderContent from '../headerContent/HeaderContent';

@observer
export default class ShopsList extends React.Component<NavigationProps> {
  render() {
    const {selectCategory, selectedCategory} = store.shopsStore;
    return (
      <View style={styles.shopsListContainer}>
        <FlatList
          ListHeaderComponent={
            <View>
              <HeaderContent navigation={this.props.navigation} />
              <HeaderText title={'Акции'} />
              <FlatList
                style={{paddingTop: 25}}
                keyExtractor={item => item.toString()}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={[1, 2, 3, 4, 5]}
                renderItem={({item}) => {
                  return (
                    <StocksListItem keyIndex={item} style={{marginLeft: 16}} />
                  );
                }}
              />
              <HeaderText title={'Магазины'} />
              <View style={styles.categoriesContainer}>
                {categories.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => selectCategory(index)}
                      key={index}
                      style={[
                        {
                          backgroundColor:
                            selectedCategory === index
                              ? '#8CC83F'
                              : 'transparent',
                        },
                        styles.categoryContainer,
                      ]}>
                      <Text
                        style={{
                          color:
                            selectedCategory === index ? '#FFFFFF' : '#000000',
                        }}>
                        {item.title}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
          }
          scrollEnabled={true}
          keyExtractor={item => item.id.toString()}
          showsVerticalScrollIndicator={true}
          data={data}
          renderItem={({item, index}) => (
            <TouchableOpacity
              // onPress={onShowShopPage}
              onPress={() => this.props.navigation.push('ShopPage')}>
              <ShopsListItem
                key={index}
                logo={item.logo}
                time={item.time}
                name={item.name}
                category={item.category}
                rating={item.rating}
                reviews={item.reviews}
              />
            </TouchableOpacity>
          )}
          ListFooterComponent={<FooterComponent />}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  shopsListContainer: {width: '100%', flex: 1},
  footerContainer: {
    backgroundColor: '#F5F4F4',
    alignItems: 'flex-start',
    paddingLeft: 30,
    flex: 1,
    paddingTop: 16,
  },
  headerTitleContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  headerTitle: {
    color: '#FFFFFF',
    fontSize: size16,
    fontFamily: MontserratRegular,
    textAlign: 'center',
  },
  categoriesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginTop: 29,
  },
  categoryContainer: {
    paddingVertical: 6,
    paddingHorizontal: 16,
    borderRadius: 10,
  },
});
