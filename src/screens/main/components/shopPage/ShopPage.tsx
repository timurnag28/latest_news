import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
// @ts-ignore
import {SuperGridSectionList} from 'react-native-super-grid';
import {observer} from 'mobx-react';
import {NavigationProps} from '../../../../share/interfaces';
import store from '../../../../stores';
import {categories, imagesPaths, shopItems} from '../../../../share/info';
import {MontserratRegular, MontserratSemiBold} from '../../../../share/fonts';
import HeaderInfo from './HeaderInfo';
import {ShopListItem} from './ShopListItem';
import FooterComponent from '../footer/FooterComponent';
import {FooterPanel} from './FooterPanel';
import {
  HEADER_HEIGHT,
  size16,
  size20,
  WINDOW_WIDTH,
} from '../../../../share/consts';
import Modal from 'react-native-modal';
import ProductPage from './productPage/ProductPage';
import MainHeader from '../../../../share/components/MainHeader';

@observer
export default class ShopPage extends React.Component<NavigationProps> {
  render() {
    const {isShowProductPage, onShowProductPage, onCloseSideBarAndShowAuth} = store.modalsStore;
    const {selectCategory, selectedCategory} = store.shopsStore;
    return (
      <View style={styles.container}>
        <View style={{height: HEADER_HEIGHT}} />
        <Modal
          animationInTiming={400}
          animationOutTiming={400}
          onBackButtonPress={onShowProductPage}
          hideModalContentWhileAnimating={true}
          backdropOpacity={0}
          onBackdropPress={onShowProductPage}
          style={{margin: 0}}
          isVisible={isShowProductPage}>
          <ProductPage />
        </Modal>
        <MainHeader
          onAuth={() => {
            onCloseSideBarAndShowAuth();
            setTimeout(() => this.props.navigation.navigate('Login'), 400);
          }}
        />
        <SuperGridSectionList
          // keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={
            <View>
              <ImageBackground
                blurRadius={10}
                style={{
                  width: WINDOW_WIDTH,
                  height: WINDOW_WIDTH / 1.1,
                }}
                source={imagesPaths.shopPageImageHeader}>
                <HeaderInfo />
              </ImageBackground>
              <View
                style={{
                  backgroundColor: '#ffffff',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderRadius: 10,
                  paddingTop: 20,
                  paddingHorizontal: 10,
                }}>
                {categories.map((item, index) => {
                  return (
                    <TouchableOpacity
                      onPress={() => selectCategory(index)}
                      key={index}
                      style={[
                        {
                          backgroundColor:
                            selectedCategory === index
                              ? '#8CC83F'
                              : 'transparent',
                        },
                        styles.categoryContainer,
                      ]}>
                      <Text
                        style={{
                          color:
                            selectedCategory === index ? '#FFFFFF' : '#000000',
                          fontSize: size16,
                          fontFamily: MontserratRegular,
                        }}>
                        {item.title}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
          }
          itemDimension={WINDOW_WIDTH / 2.5}
          sections={shopItems}
          renderItem={({item: productData}: any) => (
            <TouchableOpacity onPress={onShowProductPage}>
              <ShopListItem data={productData} />
            </TouchableOpacity>
          )}
          renderSectionHeader={({section}: any) => (
            <Text style={styles.sectionHeader}>{section.title}</Text>
          )}
          ListFooterComponent={
            <View style={{marginTop: 40}}>
              <FooterComponent />
            </View>
          }
        />
        <FooterPanel
          onPress={() => this.props.navigation.navigate('BasketPage')}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center'},
  sectionHeader: {
    color: '#BABABA',
    fontSize: size20,
    fontFamily: MontserratSemiBold,
    marginLeft: 16,
    marginTop: 45,
    marginBottom: 12,
  },
  categoriesContainer: {},
  categoryContainer: {
    paddingVertical: 16,
    paddingHorizontal: size20,
    borderRadius: 10,
  },
});
