import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {observer} from 'mobx-react';
import {
  size12,
  size14,
  size16,
  size20,
  size32,
  size44,
} from '../../../../share/consts';
import {imagesPaths} from '../../../../share/info';
import {MontserratBold, MontserratRegular} from '../../../../share/fonts';
import ShopRating from './ShopRating';

@observer
export default class HeaderInfo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.infoContainer}>
          <View style={styles.iconContainer}>
            <Image
              resizeMode={'contain'}
              style={{width: size32, height: size44}}
              source={imagesPaths.shopPageIcon}
            />
          </View>
          <Text style={styles.shopName}>Supermango</Text>
          <Text style={styles.serviceContainer}>
            Доставка овощей и фруктов · Москва
          </Text>
          <ShopRating />
          <Text style={styles.reviews}>23,467 отзыва</Text>
          <Text onPress={() => alert('подробности')} style={styles.moreDetails}>
            Подробнее
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 27,
  },
  iconContainer: {
    width: size32 * 3,
    height: size32 * 3,
    backgroundColor: '#FFC424',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  shopName: {
    marginTop: 10,
    color: '#FFFFFF',
    fontFamily: MontserratBold,
    fontSize: size20,
  },
  serviceContainer: {
    marginTop: 8,
    color: '#FFFFFF',
    fontFamily: MontserratRegular,
    fontSize: size12,
    marginBottom: 16,
  },
  reviews: {
    marginTop: 18,
    color: '#FFFFFF',
    fontFamily: MontserratRegular,
    fontSize: size16,
  },
  moreDetails: {
    marginTop: 16,
    color: 'rgba(255, 255, 255, 0.5)',
    fontFamily: MontserratRegular,
    fontSize: size14,
  },
});
