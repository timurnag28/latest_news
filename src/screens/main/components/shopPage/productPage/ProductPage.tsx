import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {imagesPaths} from '../../../../../share/info';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import store from '../../../../../stores';
import {observer} from 'mobx-react';
import {
  size14,
  size16,
  size20,
  size24,
  size34,
  size44,
  WINDOW_WIDTH,
} from '../../../../../share/consts';
import {
  MontserratBold,
  MontserratRegular,
  MontserratSemiBold,
} from '../../../../../share/fonts';

@observer
export default class ProductPage extends Component {
  state = {
    count: 0,
  };

  countUpFunction() {
    this.setState({count: this.state.count + 1});
  }

  countDownFunction() {
    this.state.count !== 0
      ? this.setState({count: this.state.count - 1})
      : null;
  }

  render() {
    const {onShowProductPage} = store.modalsStore;
    return (
      <View
        style={{
          backgroundColor: 'rgba(0, 0, 0, 0.3)',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            backgroundColor: '#ffffff',
            alignItems: 'center',
            borderRadius: 20,
            margin: 20,
          }}>
          <TouchableOpacity
            style={{marginLeft: 'auto', paddingTop: 16, paddingRight: 16}}
            onPress={onShowProductPage}>
            <EvilIcons name={'close'} size={size34 * 1.5} color={'#464646'} />
          </TouchableOpacity>
          <View style={{alignItems: 'center'}}>
            <Image
              resizeMode={'contain'}
              source={imagesPaths.tomatoes}
              style={{width: WINDOW_WIDTH / 1.5, height: WINDOW_WIDTH / 2}}
            />
            <Text style={{fontSize: size20, fontFamily: MontserratBold}}>
              Помидоры «Бакинские»
            </Text>
            <Text
              style={{
                fontSize: size14,
                fontFamily: MontserratRegular,
                paddingTop: 20,
                width: WINDOW_WIDTH * 0.7,
              }}>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no
            </Text>
          </View>
          <View
            style={{
              paddingTop: size44,
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
              width: WINDOW_WIDTH * 0.9,
              paddingBottom: size24,
            }}>
            <View style={{alignItems: 'center'}}>
              <Text style={{fontSize: size14, fontFamily: MontserratRegular}}>
                Количество
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 10,
                }}>
                <FontAwesome5
                  onPress={() => this.countDownFunction()}
                  name={'minus'}
                  size={size20}
                  color={'#8CC83F'}
                />
                <Text
                  style={{
                    fontSize: size20,
                    fontFamily: MontserratSemiBold,
                    paddingHorizontal: 11,
                  }}>
                  {this.state.count}
                </Text>
                <FontAwesome5
                  onPress={() => this.countUpFunction()}
                  name={'plus'}
                  size={size20}
                  color={'#8CC83F'}
                />
              </View>
            </View>
            <View>
              <Text style={{fontSize: size14, fontFamily: MontserratRegular}}>
                Стоимость
              </Text>
              <View style={{flexDirection: 'row', marginTop: 13}}>
                <Text
                  style={{
                    fontFamily: MontserratSemiBold,
                    fontSize: size16,
                    color: '#000000',
                  }}>
                  85 <Text style={{color: '#8CC83F'}}>₽ за</Text>
                </Text>
                <Text
                  style={{
                    fontFamily: MontserratSemiBold,
                    fontSize: size16,
                    color: '#000000',
                    marginLeft: 8,
                  }}>
                  100 <Text style={{color: '#8CC83F'}}>гр.</Text>
                </Text>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => alert('добавление в корзину')}
            style={{
              backgroundColor: '#8CC83F',
              borderRadius: 20,
              paddingVertical: 16,
              flexDirection: 'row',
              justifyContent: 'center',
              width: WINDOW_WIDTH * 0.93,
              alignItems: 'center',
            }}>
            <FontAwesome5
              name={'shopping-cart'}
              size={size16}
              color={'#FFFFFF'}
            />
            <Text style={{color: '#FFFFFF', paddingLeft: 9}}>В коризну</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
