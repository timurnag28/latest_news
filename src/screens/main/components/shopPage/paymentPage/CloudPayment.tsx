import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {size20} from '../../../../../share/consts';
import LinearGradient from 'react-native-linear-gradient';
import {MontserratSemiBold} from '../../../../../share/fonts';
import {NavigationProps} from '../../../../../share/interfaces';

export class CloudPayment extends Component<NavigationProps> {
  render() {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.props.navigation.navigate('MapPage')}
        style={{flex: 1}}>
        <LinearGradient
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
          useAngle
          angle={0}
          locations={[0, 1]}
          colors={['#A6ED4A', '#8AC83E']}>
          <Text
            style={{
              fontFamily: MontserratSemiBold,
              fontSize: size20,
              color: '#FFFFFF',
            }}>
            CLOUD PAYMENT
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}
