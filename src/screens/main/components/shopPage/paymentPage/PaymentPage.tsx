import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {observer} from 'mobx-react';
import Header from '../../../../../share/components/Header';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {CustomInput} from '../../../../../share/components/CustomInput';
import store from '../../../../../stores';
import {
  size12,
  size16,
  size20,
  size32,
  size34,
  size44,
  WINDOW_WIDTH,
} from '../../../../../share/consts';
import {
  MontserratRegular,
  MontserratSemiBold,
} from '../../../../../share/fonts';
import {PaymentElement} from './PaymentElement';
import {NavigationProps} from '../../../../../share/interfaces';

@observer
export default class PaymentPage extends Component<NavigationProps> {
  state = {
    addressInput: 'Ленинский проспект, 12',
    porchInput: '2',
    levelInput: '4',
    apartmentInput: '234',
    intercomInput: '234',
    messageToCourier:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero',
    promotionalCode: '',
  };

  render() {
    const {
      addressInput,
      porchInput,
      levelInput,
      apartmentInput,
      intercomInput,
      messageToCourier,
      promotionalCode,
    } = this.state;
    const {isSelectedPayment, onSelectPayment} = store.paymentStore;

    return (
      <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        {/*<Modal*/}
        {/*  backdropTransitionOutTiming={0}*/}
        {/*  animationIn={'slideInDown'}*/}
        {/*  animationOut={'slideOutUp'}*/}
        {/*  animationInTiming={800}*/}
        {/*  animationOutTiming={400}*/}
        {/*  onBackButtonPress={() => {*/}
        {/*    onShowMapPage();*/}
        {/*    onShowCloudPayment();*/}
        {/*  }}*/}
        {/*  hideModalContentWhileAnimating={true}*/}
        {/*  backdropOpacity={0}*/}
        {/*  onBackdropPress={onShowMapPage}*/}
        {/*  style={{margin: 0}}*/}
        {/*  isVisible={isShowMapPage}>*/}
        {/*  <MapPage />*/}
        {/*</Modal>*/}
        <Header
          style={styles.header}
          headerLeft={
            <AntDesign
              onPress={() => this.props.navigation.goBack()}
              style={{paddingLeft: 8}}
              name={'left'}
              size={size16}
              color={'#464646'}
            />
          }
          headerMid={
            <Text style={styles.headerMiddleTitle}>
              Заказы в{' '}
              <Text style={{fontFamily: MontserratSemiBold, color: '#8CC83F'}}>
                Supermango
              </Text>
            </Text>
          }
        />
        <ScrollView style={{paddingHorizontal: 26}}>
          <Text
            style={{
              color: '#BABABA',
              fontFamily: MontserratSemiBold,
              fontSize: size20,
              paddingTop: size34,
            }}>
            Адрес доставки
          </Text>
          <Text
            style={{
              fontSize: size12,
              fontFamily: MontserratRegular,
              paddingTop: 36,
              color: '#000000',
            }}>
            Адрес
          </Text>
          <CustomInput
            value={addressInput}
            onChangeText={value => this.setState({addressInput: value})}
            textInputStyle={{flex: 1}}
            style={{justifyContent: 'flex-start', marginTop: 16}}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingTop: size44,
            }}>
            <View>
              <Text style={{fontSize: size12, fontFamily: MontserratRegular}}>
                Подъезд{'    '}
              </Text>
              <CustomInput
                keyboardType={'numeric'}
                maxLength={2}
                value={porchInput}
                onChangeText={value => this.setState({porchInput: value})}
                textInputStyle={{
                  flex: 1,
                  textAlign: 'center',
                  fontFamily: MontserratRegular,
                  fontSize: size16,
                  paddingHorizontal: 6,
                }}
                style={{
                  justifyContent: 'flex-start',
                  marginTop: 16,
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: size12, fontFamily: MontserratRegular}}>
                Этаж{'          '}
              </Text>
              <CustomInput
                keyboardType={'numeric'}
                maxLength={2}
                value={levelInput}
                onChangeText={value => this.setState({levelInput: value})}
                textInputStyle={{
                  flex: 1,
                  textAlign: 'center',
                  fontFamily: MontserratRegular,
                  fontSize: size16,
                  paddingHorizontal: 8,
                }}
                style={{
                  justifyContent: 'flex-start',
                  marginTop: 16,
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: size12, fontFamily: MontserratRegular}}>
                Квартира{'     '}
              </Text>
              <CustomInput
                keyboardType={'numeric'}
                maxLength={4}
                value={apartmentInput}
                onChangeText={value => this.setState({apartmentInput: value})}
                textInputStyle={{
                  flex: 1,
                  textAlign: 'center',
                  fontFamily: MontserratRegular,
                  fontSize: size16,
                  paddingHorizontal: 8,
                }}
                style={{
                  justifyContent: 'flex-start',
                  marginTop: 16,
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: size12, fontFamily: MontserratRegular}}>
                Домофон{'     '}
              </Text>
              <CustomInput
                keyboardType={'numeric'}
                maxLength={4}
                value={intercomInput}
                onChangeText={value => this.setState({intercomInput: value})}
                textInputStyle={{
                  flex: 1,
                  paddingHorizontal: 4,
                  textAlign: 'center',
                  fontFamily: MontserratRegular,
                  fontSize: size16,
                }}
                style={{
                  justifyContent: 'flex-start',
                  marginTop: 16,
                }}
              />
            </View>
          </View>
          <Text
            style={{
              fontSize: size16,
              fontFamily: MontserratRegular,
              paddingTop: size44,
            }}>
            Сообщение для курьера
          </Text>
          <CustomInput
            multiline={true}
            numberOfLines={3}
            value={messageToCourier}
            onChangeText={value => this.setState({messageToCourier: value})}
            textInputStyle={{
              flex: 1,
              fontFamily: MontserratRegular,
              fontSize: size12,
            }}
            style={{
              justifyContent: 'flex-start',
              marginTop: 16,
              maxHeight: WINDOW_WIDTH / 4,
            }}
          />
          <Text
            style={{
              fontSize: size16,
              fontFamily: MontserratRegular,
              paddingTop: size32,
            }}>
            Способ отплаты
          </Text>
          <View>
            <View style={{flexDirection: 'row', paddingTop: 24}}>
              <PaymentElement
                title={'Онлайн'}
                paymentKey={'online'}
                isSelectedPayment={isSelectedPayment}
                onSelectPayment={(payment: string) => onSelectPayment(payment)}
              />
              <PaymentElement
                style={{marginLeft: 16}}
                title={'Картой курьеру'}
                paymentKey={'cardToCourier'}
                isSelectedPayment={isSelectedPayment}
                onSelectPayment={(payment: string) => onSelectPayment(payment)}
              />
            </View>

            <View style={{flexDirection: 'row', paddingTop: 11}}>
              <PaymentElement
                title={'Наличные'}
                paymentKey={'cash'}
                isSelectedPayment={isSelectedPayment}
                onSelectPayment={(payment: string) => onSelectPayment(payment)}
              />

              <PaymentElement
                style={{marginLeft: 16}}
                paymentKey={'applePay'}
                isSelectedPayment={isSelectedPayment}
                onSelectPayment={(payment: string) => onSelectPayment(payment)}
              />
            </View>
            <Text
              style={{
                fontSize: size16,
                fontFamily: MontserratRegular,
                paddingTop: 36,
                color: '#000000',
              }}>
              Промо-код
            </Text>
            <CustomInput
              maxLength={10}
              value={promotionalCode}
              onChangeText={value => this.setState({promotionalCode: value})}
              textInputStyle={{flex: 1}}
              style={{
                justifyContent: 'flex-start',
                marginTop: 16,
                marginBottom: size44,
              }}
            />
          </View>
        </ScrollView>
        <TouchableOpacity
          // onPress={onShowCloudPayment}
          onPress={() => this.props.navigation.navigate('CloudPayment')}
          style={{
            backgroundColor: '#8CC83F',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 27,
          }}>
          <Text
            style={{
              color: '#FFFFFF',
              fontFamily: MontserratRegular,
              fontSize: size20,
            }}>
            Оплатить заказ
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    paddingTop: size16,
    paddingBottom: 16,
    paddingLeft: 16,
  },
  headerMiddleTitle: {
    fontFamily: MontserratRegular,
    fontSize: size20,
    color: '#000000',
    paddingBottom: 4,
  },
});
