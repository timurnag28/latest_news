import {
  Image,
  StyleProp,
  StyleSheet,
  Text,
  View,
  ViewStyle,
} from 'react-native';
import React from 'react';
import {size16, WINDOW_WIDTH} from '../../../../share/consts';
import {MontserratSemiBold} from '../../../../share/fonts';

export const ShopListItem = ({
  data,
  style,
}: {
  data: {name: string; image: any};
  style?: StyleProp<ViewStyle>;
}) => {
  return (
    <View
      style={[
        {
          flex: 1,
          margin: 1,
          alignItems: 'center',
          marginHorizontal: 16,
        },
        style,
      ]}>
      <Image
        resizeMode={'contain'}
        style={{width: WINDOW_WIDTH / 2.5, height: 160}}
        source={data.image}
      />
      <View style={{flexDirection: 'column'}}>
        <Text
          style={{
            fontFamily: MontserratSemiBold,
            fontSize: size16,
            color: '#000000',
          }}>
          {data.name}
        </Text>
        <View style={{flexDirection: 'row', marginTop: 16}}>
          <Text
            style={{
              fontFamily: MontserratSemiBold,
              fontSize: size16,
              color: '#000000',
            }}>
            85 <Text style={{color: '#8CC83F'}}>₽ за</Text>
          </Text>
          <Text
            style={{
              fontFamily: MontserratSemiBold,
              fontSize: size16,
              color: '#000000',
              marginLeft: 8,
            }}>
            100 <Text style={{color: '#8CC83F'}}>гр.</Text>
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({});
