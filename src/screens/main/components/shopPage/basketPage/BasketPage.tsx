import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {observer} from 'mobx-react';
import Header from '../../../../../share/components/Header';
import {size16, size20, size34} from '../../../../../share/consts';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import {
  MontserratRegular,
  MontserratSemiBold,
} from '../../../../../share/fonts';

import BasketListItem from './BasketListItem';
import store from '../../../../../stores';
import Modal from 'react-native-modal';
import {CheckOut} from './Checkout';
import {NavigationProps} from '../../../../../share/interfaces';

@observer
export default class BasketPage extends Component<NavigationProps> {
  render() {
    const {onShowCheckOutDialog, isShowCheckOutDialog} = store.modalsStore;
    const {basketItems, productCountUp, productCountDown} = store.basketStore;
    return (
      <View style={styles.container}>
        <Modal
          backdropTransitionOutTiming={0}
          animationIn={'slideInDown'}
          animationOut={'slideOutUp'}
          animationInTiming={400}
          animationOutTiming={400}
          onBackButtonPress={onShowCheckOutDialog}
          hideModalContentWhileAnimating={true}
          backdropOpacity={0}
          onBackdropPress={onShowCheckOutDialog}
          style={{margin: 0}}
          isVisible={isShowCheckOutDialog}>
          <CheckOut />
        </Modal>
        <Header
          style={styles.header}
          headerLeft={
            <AntDesign
              style={{paddingLeft: 8}}
              onPress={() => this.props.navigation.goBack()}
              name={'left'}
              size={size16}
              color={'#464646'}
            />
          }
          headerMid={
            <Text style={styles.headerMiddleTitle}>
              Заказы в{' '}
              <Text style={{fontFamily: MontserratSemiBold, color: '#8CC83F'}}>
                Supermango
              </Text>
            </Text>
          }
          headerRight={
            <EvilIcons
              style={{paddingRight: 8}}
              onPress={onShowCheckOutDialog}
              name={'trash'}
              size={size34}
              color={'#8E8E8E'}
            />
          }
        />
        <FlatList
          scrollEnabled={true}
          keyExtractor={item => item.id.toString()}
          showsVerticalScrollIndicator={true}
          data={basketItems.slice()}
          renderItem={({item, index}) => (
            <BasketListItem
              index={index}
              key={item.id}
              data={item}
              counterUp={index => productCountUp(index)}
              counterDown={index => productCountDown(index)}
            />
          )}
          ListFooterComponent={
            <View
              style={{
                flex: 1,
                paddingBottom: size34 * 2,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 16,
                  paddingTop: 30,
                  paddingBottom: size34 * 2,
                }}>
                <Text
                  style={{fontFamily: MontserratSemiBold, fontSize: size16}}>
                  Доставка
                </Text>
                <Text style={{fontFamily: MontserratRegular, fontSize: size20}}>
                  99 <Text style={{color: '#8CC83F'}}>₽</Text>
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 16,
                  paddingTop: 30,
                  flex: 1,
                }}>
                <Text style={{fontFamily: MontserratRegular, fontSize: size16}}>
                  Время доставки
                </Text>
                <Text
                  style={{fontFamily: MontserratSemiBold, fontSize: size20}}>
                  25-30{' '}
                  <Text
                    style={{color: '#8CC83F', fontFamily: MontserratRegular}}>
                    мин
                  </Text>
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingHorizontal: 16,
                  paddingTop: 30,
                  flex: 1,
                }}>
                <Text style={{fontFamily: MontserratRegular, fontSize: size16}}>
                  Итого
                </Text>
                <Text
                  style={{fontFamily: MontserratSemiBold, fontSize: size20}}>
                  3552{' '}
                  <Text
                    style={{color: '#8CC83F', fontFamily: MontserratRegular}}>
                    ₽
                  </Text>
                </Text>
              </View>
            </View>
          }
        />
        <TouchableOpacity
          // onPress={onShowPaymentPage}
          onPress={() => this.props.navigation.navigate('PaymentPage')}
          style={{
            backgroundColor: '#8CC83F',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 27,
          }}>
          <Text
            style={{
              color: '#FFFFFF',
              fontFamily: MontserratRegular,
              fontSize: size20,
            }}>
            Оформить заказ
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  header: {
    paddingTop: size16,
    paddingBottom: 16,
    paddingLeft: 16,
  },
  headerMiddleTitle: {
    fontFamily: MontserratRegular,
    fontSize: size20,
    color: '#000000',
  },
});
