import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {size16, size20} from '../../../../../share/consts';
import {
  MontserratRegular,
  MontserratSemiBold,
} from '../../../../../share/fonts';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {observer} from 'mobx-react';

@observer
export default class BasketListItem extends Component<{
  data: {name: string; price: string; productCount: number};
  index: number;
  counterUp: (index: number) => void;
  counterDown: (index: number) => void;
}> {
  render() {
    const {data, index, counterUp, counterDown} = this.props;
    return (
      <View style={styles.rowInfoContainer}>
        <View style={{flexDirection: 'column', flex: 2, paddingLeft: 16}}>
          <Text style={{fontFamily: MontserratSemiBold, fontSize: size16}}>
            {data.name}
          </Text>
          <View style={{flexDirection: 'row', marginTop: 16}}>
            <Text style={styles.price}>
              85 <Text style={{color: '#8CC83F'}}>₽ за</Text>
            </Text>
            <Text style={styles.weight}>
              100 <Text style={{color: '#8CC83F'}}>гр.</Text>
            </Text>
          </View>
          <Text onPress={() => alert('удаление')} style={styles.deleteWord}>
            Удалить
          </Text>
        </View>
        <View style={styles.minusPlusContainer}>
          <FontAwesome5
            onPress={() => counterDown(index)}
            name={'minus'}
            size={size20}
            color={'#8CC83F'}
          />
          <Text style={styles.number}>{data.productCount}</Text>
          <FontAwesome5
            onPress={() => counterUp(index)}
            name={'plus'}
            size={size20}
            color={'#8CC83F'}
          />
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={{fontFamily: MontserratRegular, fontSize: size16}}>
            {data.price} <Text style={{color: '#8CC83F'}}>₽</Text>
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rowInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 30,
    justifyContent: 'space-around',
    borderBottomWidth: 0.5,
    borderBottomColor: '#00000029',
    paddingBottom: 14,
  },
  price: {
    fontFamily: MontserratRegular,
    fontSize: size16,
    color: '#000000',
  },
  weight: {
    fontFamily: MontserratRegular,
    fontSize: size16,
    color: '#000000',
    paddingLeft: 8,
  },
  deleteWord: {
    fontFamily: MontserratRegular,
    fontSize: size16,
    color: '#8E8E8E',
    textDecorationLine: 'underline',
    paddingTop: 8,
  },
  minusPlusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  number: {
    fontSize: size20,
    fontFamily: MontserratSemiBold,
    paddingHorizontal: 11,
  },
});
