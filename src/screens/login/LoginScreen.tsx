import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {observer} from 'mobx-react';
import {NavigationProps} from '../../share/interfaces';
import {MontserratRegular} from '../../share/fonts';
import {CustomInput} from '../../share/components/CustomInput';
import {ActionButton} from '../../share/components/ActionButton';
import {LogoAndTitle} from '../../share/components/LogoAndTitle';
import store from '../../stores';
import {
  size12,
  size16,
  size24,
  size34,
  WINDOW_HEIGHT,
} from '../../share/consts';

@observer
export default class LoginScreen extends React.Component<NavigationProps> {
  render() {
    const {phone, code, changePhone, changeCode} = store.authStore;
    const {onShowAuthPageModal} = store.modalsStore;
    return (
      <View style={styles.container}>
        <LogoAndTitle imageSize={size34 * 2} textSize={size24} />
        <View style={{alignItems: 'center'}}>
          <CustomInput
            placeholderTextColor={'#000000'}
            maxLength={10}
            keyboardType={'numeric'}
            style={{marginTop: WINDOW_HEIGHT / 5}}
            phone={true}
            placeholder={'номер телефона'}
            value={phone}
            onChangeText={item => changePhone(item)}
          />
          <CustomInput
            placeholderTextColor={'#000000'}
            keyboardType={'numeric'}
            maxLength={6}
            textInputStyle={{textAlign: 'center', marginHorizontal: 24}}
            style={{marginTop: 14}}
            placeholder={'Код из смс'}
            value={code}
            onChangeText={item => changeCode(item)}
          />
          <ActionButton
            style={{marginTop: 24}}
            onPress={() => {
              this.props.navigation.navigate('MainScreen');
            }}
            text={'Отправить СМС'}
          />
          <ActionButton
            style={{marginTop: 24}}
            onPress={() => {
              this.props.navigation.navigate('CourierScreen');
            }}
            text={'Я курьер (временно)'}
          />
          <Text style={styles.agreements}>
            Нажимая кнопку «Далее» вы {'\n'} подтверждаете что соглашаетесь с{' '}
            {'\n'}
            <Text
              onPress={() => alert('agreements')}
              style={{textDecorationLine: 'underline'}}>
              {' '}
              Пользовательским соглашением
            </Text>
          </Text>
        </View>
        <Text style={styles.licenseText}>
          Все права защищены ООО «Свежие новости» (с) 2020
        </Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  licenseText: {
    fontFamily: MontserratRegular,
    position: 'absolute',
    bottom: 32,
    fontSize: size16 / 1.5,
  },
  agreements: {
    fontFamily: MontserratRegular,
    color: '#000000',
    textAlign: 'center',
    marginTop: 28,
    fontSize: size12,
  },
});
