import {action, observable} from 'mobx';
import {Animated} from 'react-native';

class ModalsStore {
  @observable isVisibleAuthSideBar: boolean = false;
  @observable animatedValue: any = new Animated.Value(0);
  @observable isShowSideBar: boolean = false;
  @observable isShowProductPage: boolean = false;
  @observable isShowCheckOutDialog: boolean = false;
  @observable isShowReviewModal: boolean = false;
  @observable isShowMyDataModal: boolean = false;
  @observable isShowAuthPageModal: boolean = false;
  @observable isShowCourierProfileModal: boolean = false;

  @action
  onShowCourierProfileModal = () => {
    this.isShowCourierProfileModal = !this.isShowCourierProfileModal;
  };


  onCloseViewAndShowMyDataModal = () => {
    this.isShowSideBar = false;
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 300,
    }).start(this.onShowMyDataModal);
  };

  @action
  onCloseSideBarAndShowAuth = () => {
    this.isShowSideBar = false;
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 300,
    }).start();
  };

  @action
  onShowAuthModal = () => {
    this.isShowAuthPageModal = !this.isShowAuthPageModal;
  };

  @action
  onShowMyDataModal = () => {
    this.isShowMyDataModal = !this.isShowMyDataModal;
  };

  @action
  onShowReviewModal = () => {
    this.isShowReviewModal = !this.isShowReviewModal;
  };

  @action
  onShowCheckOutDialog = () => {
    this.isShowCheckOutDialog = !this.isShowCheckOutDialog;
  };

  @action
  onShowProductPage = () => {
    this.isShowProductPage = !this.isShowProductPage;
  };

  onChangeView = () => {
    if (!this.isShowSideBar) {
      this.isShowSideBar = true;
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 300,
      }).start();
    } else {
      this.isShowSideBar = false;
      Animated.timing(this.animatedValue, {
        toValue: 0,
        duration: 300,
      }).start();
    }
  };
}

const modalsStore = new ModalsStore();
export default modalsStore;
