import {action, observable} from 'mobx';
import {Animated} from 'react-native';

class ShopsStore {
  @observable selectedCategory: number = 0;
  @observable clientAddress: string =
    'Покровская площадь победы 42. Кв.202 8 пад.';
  @observable animatedValue = new Animated.Value(0);
  @observable isShowBackgroundInput: boolean = false;

  @action
  selectCategory = (index: number) => {
    this.selectedCategory = index;
  };

  @action
  onChangeClientAddress = (item: string) => {
    this.clientAddress = item;
  };

  onChangeView = () => {
    !this.isShowBackgroundInput
      ? Animated.timing(this.animatedValue, {
          toValue: 1,
          duration: 700,
          // useNativeDriver: true,
        }).start(
          () => (this.isShowBackgroundInput = !this.isShowBackgroundInput),
        )
      : Animated.timing(this.animatedValue, {
          toValue: 0,
          duration: 700,
          // useNativeDriver: true,
        }).start(
          () => (this.isShowBackgroundInput = !this.isShowBackgroundInput),
        );
  };
}

const shopsStore = new ShopsStore();
export default shopsStore;
