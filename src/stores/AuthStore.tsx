import {action, observable} from 'mobx';

class AuthStore {
  @observable phone: string = '';
  @observable code: string = '';
  @observable isAuth: boolean = false;

  @action
  changePhone = (phone: string) => {
    this.phone = phone;
  };

  @action
  changeCode = (code: string) => {
    this.code = code;
  };

  @action
  onAuth = () => {
    this.isAuth = !this.isAuth;
  };
}

const authStore = new AuthStore();
export default authStore;
