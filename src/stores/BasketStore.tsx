import {action, observable} from 'mobx';

class BasketStore {
  @observable basketItems = [
    {id: 1, name: 'Помидоры «Чери»', price: '1560', productCount: 0},
    {
      id: 2,
      name: 'Помидоры «Чери» с очень длинными названием',
      price: '240',
      productCount: 0,
    },
    {id: 3, name: 'Помидоры «Чери»', price: '565', productCount: 0},
  ];
  @observable productCount: number = 0;

  @action
  productCountUp = (index: number) => {
    this.basketItems[index].productCount += 1;
  };
  @action
  productCountDown = (index: number) => {
    this.basketItems[index].productCount !== 0
      ? (this.basketItems[index].productCount -= 1)
      : null;
  };
}

const basketStore = new BasketStore();
export default basketStore;
