import shopsStore from './ShopsStore';
import authStore from './AuthStore';
import modalsStore from './ModalsStore';
import basketStore from './BasketStore';
import paymentStore from './PaymentStore';
import locationStore from './LocationStore';

const store = {
  shopsStore,
  authStore,
  modalsStore,
  basketStore,
  paymentStore,
  locationStore,
};
export default store;
