import {action, observable} from 'mobx';

class PaymentStore {
  @observable isSelectedPayment: string = 'online';

  @action
  onSelectPayment = (value: string) => {
    this.isSelectedPayment = value;
  };
}

const paymentStore = new PaymentStore();
export default paymentStore;
