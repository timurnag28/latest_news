import React, {Component} from 'react';
import RootNavigation from './src/navigation/RootNavigation';
import {SafeAreaView, StatusBar} from 'react-native';
import {NavigationProps} from './src/share/interfaces';
import Modal from 'react-native-modal';
import MyData from './src/screens/main/components/sideBar/MyData';
import store from './src/stores';
import {observer} from 'mobx-react';
import CourierProfile from './src/screens/courier/components/CourierProfile';

@observer
export default class App extends Component<NavigationProps> {
  render() {
    const {
      onShowMyDataModal,
      isShowMyDataModal,
      onShowCourierProfileModal,
      isShowCourierProfileModal,
    } = store.modalsStore;
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar hidden={true} />
        <Modal
          animationInTiming={400}
          animationOutTiming={400}
          onBackButtonPress={onShowMyDataModal}
          hideModalContentWhileAnimating={true}
          backdropOpacity={0}
          onBackdropPress={onShowMyDataModal}
          style={{margin: 0, backgroundColor: 'transparent'}}
          isVisible={isShowMyDataModal}>
          <MyData />
        </Modal>
        {/*<Modal*/}
        {/*  animationInTiming={400}*/}
        {/*  animationOutTiming={400}*/}
        {/*  onBackButtonPress={onShowCourierProfileModal}*/}
        {/*  hideModalContentWhileAnimating={true}*/}
        {/*  backdropOpacity={0}*/}
        {/*  onBackdropPress={onShowCourierProfileModal}*/}
        {/*  style={{margin: 0, backgroundColor: '#464646'}}*/}
        {/*  isVisible={isShowCourierProfileModal}>*/}
        {/*  <CourierProfile navigation={this.props.navigation} />*/}
        {/*</Modal>*/}
        <RootNavigation />
      </SafeAreaView>
    );
  }
}
